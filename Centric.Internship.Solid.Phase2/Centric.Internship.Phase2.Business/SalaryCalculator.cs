﻿using Centric.Internship.Phase2.Interfaces;
using Centric.Internship.Phase2.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Centric.Internship.Phase2.Business
{
    public class SalaryCalculator
    {
        private IEmployeesReader _reader;
        private AverageSalaryCalculator _averageSalaryCalculator;

        public SalaryCalculator(IEmployeesReader reader)
        {
            _reader = reader;
            _averageSalaryCalculator = new AverageSalaryCalculator();
        }

        public void CalculateAverageSalary()
        {
            var employees = _reader.Read(@"..\..\..\..\SalariesPerYear.xml");

            var results = _averageSalaryCalculator.CalculateAverageSalary(employees);

            this.OutputResults(results);
        }

        private void OutputResults(Results results)
        {
            var xmlSerializer = new XmlSerializer(typeof(Results));
            using (var fileStream = new FileStream(@"..\..\..\..\ResultsPerYear.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, results);
            }
        }
    }
}
