﻿using System.Xml.Serialization;

namespace Centric.Internship.Phase2.Models
{
    [XmlRoot("Employees")]
    public class Employees
    {
        [XmlElement("Employee")]
        public Employee[] EmployeesCollection { get; set; }
    }
}